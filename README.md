# Discordance

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/discordance/main?style=plastic)](https://bitbucket.org/idomdavis/discordance/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/discordance?style=plastic)](https://bitbucket.org/idomdavis/discordance/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/discordance?style=plastic)](https://bitbucket.org/idomdavis/discordance/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/discordance)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

A wrapper around [discordgo](https://github.com/bwmarrin/discordgo) for bots.
