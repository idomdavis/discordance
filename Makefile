all: clean build lint test

build:
	go vet ./...
	go build

clean:
	go clean
	go mod tidy

lint:
	golangci-lint run

test:
	go test -covermode=count -count=1 ./...

ci: all

.PHONY: test
.DEFAULT_GOAL := all
