package message

import (
	"github.com/bwmarrin/discordgo"
)

// Handler used to process inbound messages.
type Handler func(s *discordgo.Session, msg *discordgo.Message)

// Dispatcher for inbound messages to Handlers.
type Dispatcher []Handler

// Dispatch an inbound message to the registered handlers.
func (d *Dispatcher) Dispatch(s *discordgo.Session, m *discordgo.MessageCreate) {
	for _, h := range *d {
		go h(s, m.Message)
	}
}
