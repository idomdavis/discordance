// Package discordance allows connection to discord and registration of various
// handlers for a bot interface.
package discordance
