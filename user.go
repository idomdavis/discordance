package discordance

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

// User defines basic details on a user.
type User struct {
	ID     string
	Name   string
	Avatar string
}

// Populate a User from the given session. The User ID must be set as this is
// used to retrieve the user data. If an error is returned the User struct
// remains unmodified.
func (u *User) Populate(s *discordgo.Session) error {
	user, err := s.User(u.ID)

	if err != nil {
		return fmt.Errorf("failed to get user data for %q: %w", u.ID, err)
	}

	u.Name = user.Username
	u.Avatar = user.AvatarURL("")

	return nil
}

// Author returns the user as a message embed author.
func (u *User) Author() *discordgo.MessageEmbedAuthor {
	return &discordgo.MessageEmbedAuthor{
		Name:    u.Name,
		IconURL: u.Avatar,
	}
}
