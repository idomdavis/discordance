FROM scratch

ADD discordance /
ADD avatar.png /
ADD certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["./discordance"]
