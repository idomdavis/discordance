package snowflake

import (
	"strconv"
	"time"
)

// Timestamp will return the timestamp embedded in a snowflake. Failure to
// parse the snowflake will result in the current time being returned.
func Timestamp(snowflake string) time.Time {
	const (
		offset = 22
		epoch  = 1420070400000
	)

	id, err := strconv.Atoi(snowflake)

	if err != nil {
		return time.Now()
	}

	timestamp := (id >> offset) + epoch

	return time.Unix(0, int64(timestamp)*int64(time.Millisecond))
}
