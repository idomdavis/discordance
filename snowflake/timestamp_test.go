package snowflake_test

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/idomdavis/discordance/snowflake"
)

func ExampleTimestamp() {
	fmt.Println(snowflake.Timestamp("943865945699532811").Unix())

	// Output:
	// 1645105577
}

func TestTimestamp(t *testing.T) {
	before := time.Now()
	timestamp := snowflake.Timestamp("invalid")
	after := time.Now()

	switch {
	case timestamp.Before(before):
		t.Error("Timestamp should be now")
	case timestamp.After(after):
		t.Error("Timestamp should be now")
	}
}
