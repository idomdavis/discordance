package config_test

import (
	"fmt"
	"testing"

	"bitbucket.org/idomdavis/discordance/config"
	"bitbucket.org/idomdavis/goconfigure"
)

func ExampleDiscord_Data() {
	d := &config.Discord{}

	fmt.Println(d.Data())

	// Output:
	// {UNSET }
}

func ExampleDiscord_Description() {
	d := &config.Discord{}

	fmt.Println(d.Description())

	// Output:
	// Discord Settings
}

func TestDiscord_Register(t *testing.T) {
	d := &config.Discord{}
	opts := &goconfigure.Options{}

	d.Register(opts)
}
