package config

import "bitbucket.org/idomdavis/goconfigure"

// Discord configuration block.
type Discord struct {
	Token  string
	Server string
}

// Description for the configuration block.
func (d *Discord) Description() string {
	return "Discord Settings"
}

// Register the configuration block.
func (d *Discord) Register(opts goconfigure.OptionSet) {
	opts.Add(&goconfigure.Option{
		ShortFlag:   't',
		LongFlag:    "token",
		EnvVar:      "DISCORD_TOKEN",
		ConfigKey:   "token",
		Description: "The discord token",
		Pointer:     &d.Token,
	})

	opts.Add(&goconfigure.Option{
		ShortFlag:   's',
		LongFlag:    "server",
		EnvVar:      "DISCORD_SERVER",
		ConfigKey:   "server",
		Description: "The discord server (guild) ID",
		Pointer:     &d.Server,
	})
}

// Data for the configuration block.
func (d *Discord) Data() interface{} {
	return Discord{
		Token:  goconfigure.Sanitise(d.Token, goconfigure.SET, goconfigure.UNSET),
		Server: d.Server,
	}
}
