package command

import (
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Event from Discord.
type Event struct {
	Session     *discordgo.Session
	Interaction *discordgo.InteractionCreate

	command  string
	commands []string
}

// Ephemeral flag used for event messages that should only be shown to the user.
const Ephemeral = 1 << 6

// Commands issued to get to this event. There will always be at least 1 command
// in the returned list.
func (e *Event) Commands() []string {
	var t discordgo.ApplicationCommandOptionType

	if len(e.commands) > 0 {
		return e.commands
	}

	e.commands = []string{e.Interaction.ApplicationCommandData().Name}

	opts := e.Interaction.ApplicationCommandData().Options

	for {
		switch {
		case t == discordgo.ApplicationCommandOptionSubCommand:
			return e.commands
		case len(opts) == 0:
			return e.commands
		case opts[0].Type == discordgo.ApplicationCommandOptionSubCommandGroup:
			// continue and descend
		case opts[0].Type == discordgo.ApplicationCommandOptionSubCommand:
			// continue and descend
		default:
			return e.commands
		}

		e.commands = append(e.commands, opts[0].Name)
		t = opts[0].Type
		opts = opts[0].Options
	}
}

// Command at the end of the command set.
func (e Event) Command() string {
	if e.command == "" {
		c := e.Commands()

		e.command = c[len(c)-1]
	}

	return e.command
}

// User ID of the user who initiated this command.
func (e Event) User() string {
	if e.Interaction.User != nil {
		return e.Interaction.User.ID
	}

	if e.Interaction.Member != nil {
		return e.Interaction.Member.User.ID
	}

	return ""
}

// Respond to an event.
func (e Event) Respond(response *discordgo.InteractionResponse) {
	i := e.Interaction.Interaction

	if err := e.Session.InteractionRespond(i, response); err != nil {
		logrus.WithError(err).Error(
			"Failed to respond to interaction")
	}
}
