package command

import (
	"github.com/bwmarrin/discordgo"
)

// Handler for a command.
type Handler func(event Event) error

// Definition of a command providing all the details required to register and
// handle it.
type Definition struct {
	Name        string
	Description string
	Handler     Handler

	SubCommands []*Definition
	Options     []*discordgo.ApplicationCommandOption
}

// Translate the Definition to the internal discordgo format.
func (c Definition) Translate() *discordgo.ApplicationCommand {
	ac := &discordgo.ApplicationCommand{
		Name:        c.Name,
		Description: c.Description,
	}

	if len(c.Options) > len(c.SubCommands) {
		ac.Options = make([]*discordgo.ApplicationCommandOption, len(c.Options))
	} else {
		ac.Options = make([]*discordgo.ApplicationCommandOption, len(c.SubCommands))
	}

	for i, o := range c.Options {
		ac.Options[i] = o
	}

	for i, o := range c.translate() {
		ac.Options[i] = o
	}

	return ac
}

func (c Definition) translate() []*discordgo.ApplicationCommandOption {
	opts := make([]*discordgo.ApplicationCommandOption, len(c.SubCommands))

	for i, o := range c.SubCommands {
		opts[i] = &discordgo.ApplicationCommandOption{
			Name:        o.Name,
			Description: o.Description,
			Type:        discordgo.ApplicationCommandOptionSubCommandGroup,
		}

		if o.Handler != nil {
			opts[i].Type = discordgo.ApplicationCommandOptionSubCommand
		}

		opts[i].Options = o.translate()
	}

	return opts
}
