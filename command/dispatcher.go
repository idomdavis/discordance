package command

import (
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Dispatcher contains the set of handlers which it will Dispatch interactions
// to.
type Dispatcher map[string]Handler

// Dispatch an interaction, either to a top level command, or a bottom level
// sub command.
func (d Dispatcher) Dispatch(event Event) {
	msg := &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: "Sorry, something went wrong!", Flags: Ephemeral},
	}

	if handler, ok := d[event.Command()]; !ok {
		logrus.WithField("event", event).Error("Failed to dispatch event")
		event.Respond(msg)
	} else if err := handler(event); err != nil {
		logrus.WithError(err).WithField("event", event).Error("Failed to handle event")
		event.Respond(msg)
	}
}

// Register a command definition with the dispatcher.
func (d Dispatcher) Register(definition *Definition) {
	if definition.Handler != nil {
		d[definition.Name] = definition.Handler
	}

	for _, c := range definition.SubCommands {
		d.Register(c)
	}
}
