package main

import (
	"bitbucket.org/idomdavis/discordance"
	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/discordance/config"
	"bitbucket.org/idomdavis/goconfigure"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

var cmd = &command.Definition{
	Name:        "cmd",
	Description: "Test Command",
	Handler:     cmdHandler,
}

func main() {
	var settings config.Discord

	logrus.SetFormatter(&logrus.TextFormatter{
		DisableLevelTruncation: true,
		FullTimestamp:          true,
		PadLevelText:           true,
	})

	s := goconfigure.Settings{}

	s.AddHelp()
	s.AddConfigFile()
	s.Add(&settings)

	if err := s.Parse(goconfigure.LogrusReporter{}); err != nil {
		logrus.WithError(err).Fatal("Failed to configure bot")
	}

	c, err := discordance.Connect(settings)

	if err != nil {
		logrus.WithError(err).Fatal("Failed to connect")
	}

	c.Unregister(settings.Server)

	c.Commands(settings.Server, cmd)
	c.Message(msgHandler)
	c.Listen()
}

func cmdHandler(event command.Event) error {
	response := &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: "Commands working!", Flags: command.Ephemeral},
	}

	event.Respond(response)

	return nil
}

func msgHandler(s *discordgo.Session, in *discordgo.Message) {
	if in.Content == "bot test" {
		out, err := s.ChannelMessageSendComplex(in.ChannelID, &discordgo.MessageSend{
			Content: "Message handlers working!",
		})

		if err != nil {
			logrus.WithError(err).Error("Failed to send message")
		}

		edit := discordgo.NewMessageEdit(out.ChannelID, out.ID)
		edit.SetContent("Message edits also working!")

		_, err = s.ChannelMessageEditComplex(edit)

		if err != nil {
			logrus.WithError(err).Error("Failed to edit message")
		}
	}
}
