package discordance

import (
	"fmt"
	"os"
	"os/signal"

	"bitbucket.org/idomdavis/discordance/command"
	"bitbucket.org/idomdavis/discordance/config"
	"bitbucket.org/idomdavis/discordance/message"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Connection to a discord server.
type Connection struct {
	*discordgo.Session

	dispatcher command.Dispatcher
}

// Connect to discord using the given config.
func Connect(settings config.Discord) (*Connection, error) {
	c := &Connection{}
	s, err := discordgo.New("Bot " + settings.Token)

	if err != nil {
		return c, fmt.Errorf("failed to connect to discord: %w", err)
	}

	c.Session = s

	if err = c.Session.Open(); err != nil {
		return c, fmt.Errorf("failed to open session: %w", err)
	}

	return c, nil
}

// Commands to register on the given server.
func (c *Connection) Commands(server string, commands ...*command.Definition) {
	id := c.Session.State.User.ID

	if c.dispatcher == nil {
		c.dispatcher = command.Dispatcher{}
	}

	for _, cmd := range commands {
		ac := cmd.Translate()

		if _, err := c.Session.ApplicationCommandCreate(id, server, ac); err != nil {
			logrus.WithError(err).WithField("command", cmd.Name).Error(
				"Failed to register command")
		} else {
			c.dispatcher.Register(cmd)
		}
	}
}

// Message handlers to register on the connection.
func (c *Connection) Message(handlers ...message.Handler) {
	d := message.Dispatcher(handlers)

	c.Session.AddHandler(d.Dispatch)
}

// Listen for events. Listen blocks until SIGINT is received.
func (c *Connection) Listen() {
	if c.dispatcher != nil && len(c.dispatcher) > 0 {
		c.Session.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			c.dispatcher.Dispatch(command.Event{Session: s, Interaction: i})
		})
	}

	stop := make(chan os.Signal, 1)

	signal.Notify(stop, os.Interrupt)

	<-stop

	_ = c.Session.Close()
}

// Unregister commands on this server.
func (c *Connection) Unregister(server string) {
	id := c.Session.State.User.ID
	commands, err := c.Session.ApplicationCommands(id, server)

	if err != nil {
		logrus.WithError(err).WithField("server", server).Error(
			"Failed to list registered command")
	}

	for _, cmd := range commands {
		if err = c.Session.ApplicationCommandDelete(id, server, cmd.ID); err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"server":  server,
				"command": cmd.ID,
			}).Error(
				"Failed to list registered command")
		}
	}
}
