module bitbucket.org/idomdavis/discordance

go 1.16

require (
	bitbucket.org/idomdavis/goconfigure v0.5.9
	github.com/bwmarrin/discordgo v0.23.3-0.20211010150959-f0b7e81468f7
	github.com/sirupsen/logrus v1.8.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211025112917-711f33c9992c // indirect
)
